package com.exadel.gs.test.service;

import com.exadel.gs.test.model.Post;
import org.openspaces.core.GigaSpace;
import org.openspaces.remoting.RemotingService;

import javax.annotation.Resource;

/**
 * @author Andrey Frunt
 */
@RemotingService
public class PostServiceImpl implements PostService {
    @Resource
    private GigaSpace gigaSpace;

    @Override
    public void create(Post post) {
        gigaSpace.write(post);
    }

    @Override
    public void remove(Post post) {
        gigaSpace.take(post);
    }

    @Override
    public Post[] list() {
        return gigaSpace.readMultiple(new Post());
    }
}
