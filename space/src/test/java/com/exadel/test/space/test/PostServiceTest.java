package com.exadel.test.space.test;

import com.exadel.gs.test.model.Post;
import com.exadel.gs.test.service.PostService;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openspaces.core.GigaSpace;
import org.openspaces.remoting.ExecutorProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/META-INF/spring/pu.xml")
public class PostServiceTest {
    @ExecutorProxy
    private PostService postService;
    @Resource
    private GigaSpace gigaSpace;

    @Test
    public void overalTest() {
        postService.create(new Post(1L, new Date(), "username", "text"));
        postService.create(new Post(2L, new Date(), "username", "text"));
        postService.create(new Post(3L, new Date(), "username", "text"));
        Assert.assertEquals("There should be only one post in space", 3, gigaSpace.count(new Post()));
        Post[] posts = postService.list();
        Assert.assertEquals("Service must return 3 posts", 3, posts.length);
        for (Post post : posts) {
            postService.remove(post);
        }
        Assert.assertEquals("There should be no posts in space", 0, gigaSpace.count(new Post()));
    }
}
