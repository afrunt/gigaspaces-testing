package com.exadel.gs.test.model;

import com.gigaspaces.annotation.pojo.SpaceClass;
import com.gigaspaces.annotation.pojo.SpaceId;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Andrey Frunt
 */
@SpaceClass
public class Post implements Serializable {
    private Long id;
    private Date date;
    private String username;
    private String text;

    public Post() {
    }

    public Post(Long id, Date date, String username, String text) {
        this.id = id;
        this.date = date;
        this.text = text;
        this.username = username;
    }

    @SpaceId(autoGenerate = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
