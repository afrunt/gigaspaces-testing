package com.exadel.gs.test.service;

import com.exadel.gs.test.model.Post;

/**
 * @author Andrey Frunt
 */
public interface PostService {
    void create(Post post);

    void remove(Post post);

    Post[] list();
}
