package com.exadel.gs.test.web.service;

import com.exadel.gs.test.model.Post;
import com.exadel.gs.test.service.PostService;
import org.openspaces.remoting.ExecutorProxy;
import org.springframework.stereotype.Service;

/**
 * @author Andrey Frunt
 */
@Service
public class PostServiceBridge {
    @ExecutorProxy
    private PostService postService;

    public void create(Post post) {
        postService.create(post);
    }

    public void remove(Post post) {
        postService.remove(post);
    }

    public Post[] list() {
        return postService.list();
    }
}
