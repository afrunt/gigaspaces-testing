package com.exadel.gs.test.web.controller;


import com.exadel.gs.test.model.Post;
import com.exadel.gs.test.web.service.PostServiceBridge;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping(value = {"/", ""})
public class PostController {
    private static long idCnt = 0L;
    @Resource
    private PostServiceBridge postServiceBridge;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String list(Model uiModel) {
        uiModel.addAttribute("posts", postServiceBridge.list());
        return "index";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@RequestParam String username, @RequestParam String text) {
        Post post = new Post();
        post.setUsername(username);
        post.setText(text);
        post.setId(idCnt++);
        postServiceBridge.create(post);
        return "redirect:/";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.POST)
    public String remove(@RequestParam Long id) {
        Post post = new Post();
        post.setId(id);
        postServiceBridge.remove(post);
        return "redirect:/";
    }
}
