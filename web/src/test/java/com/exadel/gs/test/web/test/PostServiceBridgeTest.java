package com.exadel.gs.test.web.test;

import com.exadel.gs.test.model.Post;
import com.exadel.gs.test.web.service.PostServiceBridge;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openspaces.core.GigaSpace;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/META-INF/spring/pu.xml")
public class PostServiceBridgeTest {
    @Resource
    private GigaSpace gigaSpace;
    @Resource
    private PostServiceBridge postServiceBridge;

    @Test
    public void overalTest() {
        postServiceBridge.create(new Post(1L, new Date(), "username", "text"));
        postServiceBridge.create(new Post(2L, new Date(), "username", "text"));
        postServiceBridge.create(new Post(3L, new Date(), "username", "text"));
        Assert.assertEquals("There should be only one post in space", 3, gigaSpace.count(new Post()));
        Post[] posts = postServiceBridge.list();
        Assert.assertEquals("Service must return 3 posts", 3, posts.length);
        for (Post post : posts) {
            postServiceBridge.remove(post);
        }
        Assert.assertEquals("There should be no posts in space", 0, gigaSpace.count(new Post()));
    }
}
